from django.shortcuts import render
import logging
# 创建日志记录器
logger = logging.getLogger('log')

def index(request):
    #使用日志记录信息
    logger.info('记录info日志')
    logger.error('记录error日志')

    context = {'value': "这是jinja2"}
    return render(request, 'index.html', locals())